# We could add extra obelisks in here if we want to CI against other obelisk combos
# As of right now, I think Kiln is stuck on an obelisk that has ghc 8.4 and other 
# tbml users are using newer versions that have 8.6., so it could be worth doing this
# earlier rather than later. 
import ./. {}
