**NOTE: THIS REPOSITORY IS DEPRECATED AND IS NO LONGER MANTAINED**

This library has been updated for new protocols for use in the [Kiln](https://tezos-kiln.org/) project, its source code has been moved to the [kiln repository](https://gitlab.com/tezos-kiln/kiln) and will be further developed and updated within it.

# Tezos Bake Monitor Lib

This is a haskell library for interacting with a Tezos RPC.

The api is not guaranteed to be stable nor is it very well documented for public consumption, but it is MIT licensed so please feel free to use it if it is useful to you!

## The library comes in two parts

- tezos-bake-monitor-lib: All of the JSON serialisation types and code that is GHCJs friendly.
- tezos-noderpc: The code that will actually do HTTP calls to a tezos node.

If you only have backend code, you'll just need to use noderpc. If you have a GHCJs frontend and want to send tezos types to the frontend, then import tbml into your frontend.
