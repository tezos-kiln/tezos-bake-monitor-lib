module Tezos.V006.Unsafe (module X) where

import Tezos.V006.ProtocolConstants as X (unsafeAssumptionLevelToCycle, unsafeAssumptionFirstLevelInCycle, unsafeAssumptionRightsContextLevel)
