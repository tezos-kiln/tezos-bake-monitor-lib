{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Tezos.V006.Tez (module Old) where
import Tezos.V005.Tez as Old

import Data.Int (Int64)
import Tezos.Common.Json (StringEncode(StringEncode))
import Tezos.V006.Micheline (Expression(Expression_Int))
import Tezos.V006.Michelson (FromMicheline(fromMicheline), ToMicheline(toMicheline))

instance ToMicheline TezDelta where
  toMicheline = Expression_Int . StringEncode . toInteger . getMicroTezDelta

instance FromMicheline TezDelta where
  fromMicheline = \case
    Expression_Int (StringEncode x)
      | x >= toInteger (minBound :: Int64)
      , x <= toInteger (maxBound :: Int64) -> Right $ microTezDelta $ fromInteger x
      | otherwise -> Left "mutez amount out of bounds"
    _ -> Left "unexpected encoding for mutez"