module Tezos.V005.Unsafe (module X) where

import Tezos.V005.ProtocolConstants as X (unsafeAssumptionLevelToCycle, unsafeAssumptionFirstLevelInCycle, unsafeAssumptionRightsContextLevel)
